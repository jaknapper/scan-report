from PIL import Image
import numpy as np
import time
import matplotlib.pyplot as plt
from matplotlib import cm
import cv2
from scipy import ndimage
import os
import piexif
import json
import logging
import yaml
from collections import Counter
from dataclasses import dataclass
import datetime
from mpl_toolkits.mplot3d import Axes3D

dir = r'C:\Users\jakna\Source\training_data_reports\summary'

scan_count = 0
correct_peaks = 0 
undershot_peaks = 0 
overshot_peak_count = 0 
missed_peak_count = 0 
messy_peak_count = 0 

for filename in os.listdir(dir):
    if filename.endswith(".txt"):
        with open(os.path.join(dir,filename)) as f:
            data = json.load(f)
            scan_count += data['scan_count']
            for i in range(data['scan_count']):
                if (data['scan{0}'.format(i)]['grid_shape'][2]) >= 5:
                    correct_peaks += data['scan{0}'.format(i)]['stack_data']['correct peaks']
                    undershot_peaks += data['scan{0}'.format(i)]['stack_data']['undershot peak count']
                    overshot_peak_count += data['scan{0}'.format(i)]['stack_data']['overshot peak count']
                    missed_peak_count += data['scan{0}'.format(i)]['stack_data']['missed peak count']
                    messy_peak_count += data['scan{0}'.format(i)]['stack_data']['messy peak count']
    else:
        continue

print(scan_count)
print(correct_peaks, undershot_peaks, overshot_peak_count, missed_peak_count, messy_peak_count)