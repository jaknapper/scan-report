from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
from scipy import ndimage
import os
import glob
import json
from collections import Counter
from dataclasses import dataclass
import datetime
from . import metadata_to_pd
from . import parameters
import itertools
import time
import argparse
import traceback


def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def flatten(nested):
    flat = itertools.chain.from_iterable
    return list(flat(nested))

@dataclass
class Capture:
    # Class for each image with only the useful data
    name: str
    x: float
    y: float
    z: float
    capture_time: datetime.datetime
    image_size: float
    scan_id: str
    scan_time: str
    capture_fail: bool
    meta_format: str

def get_time(e):
    return e.capture_time.time()

def folder_report(dir, data_folder_root, reports_folder_name, plot_path, image_tester=False, overwrite_blanks = True):
    #image tester = true will check each image for a failed capture, significantly slowing the report
    
    if dir.startswith(data_folder_root):
        folder_link = dir[len(data_folder_root):]
    else:
        raise Exception("We got a folder that doesn't appear to be from the right place!")

    if not os.path.exists(os.path.join(reports_folder_name, folder_link)):
        os.makedirs(os.path.join(reports_folder_name, folder_link))

    output_dir = os.path.join(reports_folder_name, folder_link)

    scan_lists = image_processing(dir, output_dir, image_tester=image_tester, overwrite_blanks = overwrite_blanks)

    name_all_scans,x_all_scans,y_all_scans,z_all_scans,size_all_scans,scan_id_all_scans,time_all_scans = produce_report(scan_lists, dir, output_dir, image_tester=image_tester)

    if plot_path is True:
        plot_scans(output_dir, x_all_scans, y_all_scans)

    return(name_all_scans,x_all_scans,y_all_scans,z_all_scans,size_all_scans,scan_id_all_scans,time_all_scans)

def image_processing(dir, output_dir, image_tester = False, overwrite_blanks = True):

    # a list of all Capture objects and all scan start times

    image_d_list = []
        
    # for all files in the directory ending in .jpeg, load the metadata and append an Capture object to image_d_list
    
    image_list = [f for f in os.listdir(dir) if f[-5:] == '.jpeg']
    for item in image_list:
        link = (os.path.join(dir, item))
        metadata, meta_format = metadata_to_pd.pulldata(link, parameters.all_data)
        try:
            metadata = metadata_to_pd.estimate_image_metadata(link, metadata, overwrite_blanks)
        except:
            if item == image_list[0]:
                print('Can\'t estimate meta')
            pass
        

        if image_tester is True:
            im = Image.open(link)
            if not im.getbbox(): failed = True
            else: failed = False
        else: failed = False

        if item == image_list[0]:
            (np.transpose(metadata)).to_csv(os.path.join(output_dir, 'example_meta.csv'))

        image_d_list.append(Capture(
            name = metadata['image_name'][0],
            x = metadata['instrument_state_stage_position_x'][0],
            y = metadata['instrument_state_stage_position_y'][0],
            z = metadata['instrument_state_stage_position_z'][0],
            capture_time = datetime.datetime.strptime(metadata["image_acquisitionDate"][0], '%Y-%m-%d_%H-%M-%S'),
            image_size = metadata['image_size'][0],
            scan_id = metadata['dataset_id'][0],
            scan_time = metadata['dataset_acquisitionDate'][0],
            capture_fail = failed,
            meta_format = meta_format
            )
        )

    # sort the list by image capture time (allowing us to see grid shapes)

    image_d_list.sort(key=get_time)
    prev_time = 0
    start_scan_time = 0
    scan_times = []



    for images in image_d_list:
        if images.scan_time in [None, 0]:
            if str(images.capture_time) == str(image_d_list[0].capture_time):
                start_scan_time = images.capture_time
                images.scan_time = start_scan_time
            else: 
                dt = images.capture_time
                unix_dt = dt.replace(tzinfo=datetime.timezone.utc).timestamp()
                if unix_dt - prev_time.replace(tzinfo=datetime.timezone.utc).timestamp() > 60:
                    start_scan_time = images.capture_time
                    images.scan_time = start_scan_time
                else:
                    images.scan_time = start_scan_time
        else: start_scan_time = images.scan_time
        prev_time = images.capture_time
        scan_times.append(images.scan_time)

    

    # get a list of just the unique scan times, and split image_d_list into scan_lists arrays for each start time

    times = list(set(scan_times))
    scan_lists = [ [] for i in range(len(times))]
    for arrays in range(len(times)):
        scan_lists[arrays] = [i for i in image_d_list if (i.scan_time) == times[arrays]]

    return scan_lists

def plot_scans(output_dir, x_all_scans, y_all_scans):
    fig, ax = plt.subplots()
    for scans in range(len(x_all_scans)):
        for arrows in range(len(x_all_scans[scans])-1):
            ax.annotate("", xy=(x_all_scans[scans][arrows+1],y_all_scans[scans][arrows+1]), xytext=(x_all_scans[scans][arrows],y_all_scans[scans][arrows]), arrowprops=dict(headwidth = 5, width = 0.2))
            plt.plot(x_all_scans[scans],y_all_scans[scans],'or')
    
    ax.set_xlabel('x (steps)')
    ax.set_ylabel('y (steps)')
    plt.savefig(os.path.join(output_dir,'xy_paths.png'),bbox_inches = "tight")
    plt.clf()
    plt.close(fig)

def produce_report(scan_lists, dir, output_dir, image_tester):

    report = ''
    folder_name = '{0}_{1}'.format(dir.split(os.sep)[-2], dir.split(os.sep)[-1])
    folder_link = os.path.join(dir.split(os.sep)[-2], dir.split(os.sep)[-1])

    # if not os.path.exists(os.path.join(dir,'report')):
    #     os.makedirs(os.path.join(dir,'report'))

    report += 'Folder name is {0}\n'.format(folder_name)
    report += 'Folder path is {0}\n'.format(dir)
    report += 'There are {0} scan(s) in this folder\n'.format(len(scan_lists))

    # prepare a report for each scan

    scan_dict= [[] for arrays in range(len(scan_lists))]
    x_all_scans = []
    y_all_scans = []
    z_all_scans = []
    name_all_scans = []
    size_all_scans = []
    scan_id_all_scans = []
    time_all_scans = []

    for arrays in range(len(scan_lists)):

        report += '\nScan {0}:\n'.format(arrays+1) 

        initial_x = scan_lists[arrays][0].x # starting and final grid positions
        initial_y = scan_lists[arrays][0].y
        initial_z = scan_lists[arrays][0].z

        rows = 0
        cols = 0
        stack_height = 0
        style = None

        x_all = []
        y_all = []
        z_all = []
        size_all = []
        name_all = []
        scan_id_all = []
        time_all = []
        metaformat = scan_lists[arrays][0].meta_format

        # make a list of each variable we might want to sort, plot, etc

        for count in range(len(scan_lists[arrays])):
            images = scan_lists[arrays][count]
            name_all.append(images.name)
            x_all.append(images.x)
            y_all.append(images.y)
            z_all.append(images.z)
            size_all.append(images.image_size)
            scan_id_all.append(images.scan_id)
            time_all.append((images.capture_time).strftime("%m/%d/%Y_%H:%M:%S"))
            if images.capture_fail is True:
                report += 'Image {0} seems to have failed in some way\n'.format(images.name)
                print('Image {0} seems to have failed in some way\n'.format(images.name))
                any_failed = True

            # get the number of images in a z stack by counting the number of images taken at the starting x-y coordinate pair
            
            if images.x == initial_x and images.y == initial_y: stack_height += 1
        
        # number of rows and columns are the number of unique widths and heights

        rows = len(set(y_all))
        cols = len(set(x_all))

        # there should only be 3 possible movements in y: 0, +/-dy or the raster scan moving from bottom of column
        # to the top of the next. the most common change in y will be dy
        # we'll check that the first change in y that isn't 0 is also dy, and that there are only 3 changes in y
        try:
            dy = (np.diff(y_all))
            dy = dy[dy != 0]
            first_dy = dy[0]
            dy = Counter(dy)
            dy = dy.most_common(1)[0][0]
            if dy != first_dy or len(Counter(np.diff(y_all))) > 3:
                print('This scan seems to be a weird shape. Check the plot')
                report += 'This scan seems to be a weird shape. Check the plot\n'
        except:
            print ('Only completed one line, can\'t know dy')
            dy = 0
        # if there's a change in y greater than dy, it must be a raster move returning to the top

        if max(abs((np.diff(y_all)))) > dy: style = "raster"
        else: style = "snake"

        # same for dx, but without the raster scan complication
        try:
            dx = (np.diff(x_all))
            dx = dx[dx != 0]
            first_dx = dx[0]
            dx = Counter(dx)
            dx = dx.most_common(1)[0][0]
            if dx != first_dx or len(Counter(np.diff(x_all))) > 2:
                print('This scan seems to be a weird shape. Check the plot')
                report += 'This scan seems to be a weird shape. Check the plot\n'
        except:
            print ('Only completed one image, can\'t know dx')
            dx = 0
        # dz is the difference in z between the first and second images
        # unless the scan didn't take z stacks, in which case we can call dz = 0 (could be anything)

        if stack_height != 1: dz = z_all[1] - z_all[0]
        else: dz = 0

        report += 'Scan ID: {0}. Grid steps are {1}, {2}, {3}, starting at {4}, {5}, {6}. Style is {7}.\n'.format(scan_lists[arrays][0].scan_id, str(dx), str(dy), str(dz), str(initial_x), str(initial_y), str(initial_z), style)

        report += 'Grid shape is {0},{1},{2}. Total coordinate points is {3}. Total image count is {4}.\n'.format(str(cols), str(rows), str(stack_height), str(rows*cols), str(len(x_all)))

        report += 'Metadata format is {0}.\n'.format(metaformat)

        if stack_height == 1:
            pass
        elif stack_height >= 3 and stack_height % 2 != 0:
            new_z_all = []
            new_size_all = []
            centre_index = stack_height // 2

            success_count = 0

            # undershot is if the peak is at a higher height than the z stack. we'll swap them later if necessary
            undershot_count = 0
            overshot_count = 0

            missed_peak_count = 0
            messy_peak_count = 0

            missed_dist = []


            for i in range(0, len(z_all), stack_height):
                new_z_all.append(z_all[i : i + stack_height])
                new_size_all.append(size_all[i : i + stack_height])
                max_height_index = np.argmax(new_size_all[int(i/stack_height)])
                missed_dist.append(max_height_index - centre_index)
                # plt.plot(z_all[i : i + stack_height], size_all[i : i + stack_height])
                # plt.show()
                # plt.clf()
                if size_all[i + centre_index] == max(size_all[i : i + stack_height]): ##TODO check if images either side are in order
                    # print('Success')
                    success_count += 1
                elif sorted(size_all[i : i + stack_height]) == size_all[i : i + stack_height]:
                    undershot_count += 1
                    # print('Undershot')
                elif sorted(size_all[i : i + stack_height], reverse=True) == size_all[i : i + stack_height]:
                    overshot_count += 1
                    # print('Overshot')

                elif sorted(size_all[i : i + max_height_index], reverse=True) == size_all[i : i + max_height_index] and sorted(size_all[i+max_height_index : i + stack_height]) == size_all[i+max_height_index : i + stack_height]:
                    # print('Missed peak')
                    missed_peak_count += 1

                elif sorted(size_all[i : i + max_height_index]) == size_all[i : i + max_height_index] and sorted(size_all[i+max_height_index : i + stack_height], reverse=True) == size_all[i+max_height_index : i + stack_height]:
                    # print('Missed peak')
                    missed_peak_count += 1

                else:
                    messy_peak_count += 1
                    # print('Messy peak')
                
                # if the z stacks are moving downwards, undershot and overshot should be swapped
                if dz < 0: undershot_count, overshot_count = overshot_count, undershot_count

            report += 'Number of images the peak was away from each centre image was {0}\n'.format(str(dict(Counter(missed_dist))))
            report += 'Good peak count = {0}\n'.format(str(success_count))
            report += 'Undershot peak count = {0}\n'.format(str(undershot_count))
            report += 'Overshot peak count = {0}\n'.format(str(overshot_count))
            report += 'Missed peak count = {0}\n'.format(str(missed_peak_count))
            report += 'Messy peak count = {0}\n'.format(str(messy_peak_count))

        if image_tester is True and any_failed is False:
            report += 'Looks like all images were captured correctly\n'
        elif image_tester is False:
            report += 'Image capture success not checked\n'

        if len(x_all) < rows * cols * stack_height:
            report += 'This scan wasn\'t complete. Grid requires {0} images, only {1} were taken\n'.format(rows*cols*stack_height,len(x_all))
        elif len(x_all) > rows * cols * stack_height:
            report += 'There\'s more images here than suggested by the grid. Grid requires {0} images, but {1} were taken\n'.format(rows*cols*stack_height,len(x_all))
        
        scan_dict[int(arrays)] = {
            'style': style,
            'scan_ID': scan_lists[arrays][0].scan_id,
            'dx': int(dx),
            'dy': int(dy),
            'dz': int(dz),
            'initial_pos': [int(initial_x), int(initial_y), int(initial_z)],
            'grid_shape': [cols, rows, stack_height],
            'grid total images': (cols*rows*stack_height),
            'total_images': len(x_all),
            'meta_format': metaformat
        }
        if stack_height >= 3:
            scan_dict[int(arrays)]['stack_data'] = {
                'peak_differences': str((Counter(missed_dist))),
                'correct peaks': success_count,
                'undershot peak count': undershot_count,
                'overshot peak count': overshot_count,
                'missed peak count': missed_peak_count,
                'messy peak count': messy_peak_count
            }

        x_all_scans.append(x_all)
        y_all_scans.append(y_all)
        z_all_scans.append(z_all)
        size_all_scans.append(size_all)
        name_all_scans.append(name_all)
        scan_id_all_scans.append(scan_id_all)
        time_all_scans.append(time_all)

    if len(scan_lists) > 1: 
        csv_data = np.vstack((flatten(name_all_scans), flatten(x_all_scans), flatten(y_all_scans), flatten(z_all_scans), flatten(size_all_scans), flatten(scan_id_all_scans), flatten(time_all_scans)))
    else:
        csv_data = np.vstack((name_all_scans, x_all_scans, y_all_scans, z_all_scans, size_all_scans, scan_id_all_scans, time_all_scans))
    np.savetxt(os.path.join(output_dir,'data.csv'), np.transpose(csv_data), delimiter=",", fmt='%s')

    with open(os.path.join(output_dir,'report.txt'), 'w') as reader:
        reader.write(report)
    
    JSON_report = {
        "comments": report,
        "scan_count": len(scan_lists)
    }

    for scans in range(len(scan_lists)):
        JSON_report['scan{0}'.format(int(scans))] = scan_dict[scans]
    
    # with open(os.path.join(output_dir, '{0}.txt'.format(folder_name)), 'w') as outfile:
    #     json.dump(JSON_report, outfile)

    with open(os.path.join(output_dir,'report.json'), 'w') as outfile:
        json.dump(JSON_report, outfile)

    return(name_all_scans,x_all_scans,y_all_scans,z_all_scans,size_all_scans,scan_id_all_scans,time_all_scans)

def report_all_folders(dir, reports_folder_name, plot_path, image_tester, overwrite_blanks):

    # We keep track of progress in a file with one line par non-empty folder
    progress_fname = os.path.join(reports_folder_name, "FOLDERS_PROCESSED")

    # If this file does not exist, we've not previously processed any folders
    if not os.path.isfile(progress_fname):
        folders_processed = []
    else:
        with open(progress_fname, "r") as f:
            folders_processed = f.readlines()
                
    bad_folder = 0
    empty_folder = 0
    total_folder = 0
    try:
        for folder, subfolders, files in os.walk(dir):  # Recursively visit every subfolder of `dir`
            if folder.endswith("@__thumb"):
                # Skip thumbnail folders, don't even count them!
                continue
            total_folder += 1

            if folder in folders_processed:
                # Silently skip folders we've processed before
                continue

            n_jpegs = len(glob.glob1(folder, "*.jp*g"))  # Could probably use `files` for this...
            if n_jpegs == 0:
                print(f"Folder {folder} contains no images, skipping.")
                folders_processed.append(folder)
                empty_folder += 1
                continue
            print(f"Folder {folder} contains {n_jpegs} images, generating report.")
            try:
                folder_report(folder, dir, reports_folder_name, plot_path = plot_path, image_tester = image_tester,overwrite_blanks=overwrite_blanks)
                folders_processed.append(folder)
            except KeyboardInterrupt as e:
                raise e
            except Exception as e:
                bad_folder += 1
                print(f"Failure! {folder}")
                traceback.print_exc()
    finally:
        with open(progress_fname, "w") as f:
            for fp in folders_processed:
                f.write(fp + "\n")

    print("Number of unreadable subfolders is " + str(bad_folder))
    print("Number of empty subfolders is " + str(empty_folder))
    print("Total number of folders is " + str(total_folder))

# start = time.time()

# reports_folder_name = r"E:\scans_report"
# # dir = r"C:\Users\jakna\Source\training_data"
# dir = r"E:\scans"
# report_all_folders(dir, reports_folder_name, plot_path = True,  image_tester = False, overwrite_blanks = True)

# print(time.time() - start)

# reports_folder_name = r"E:\scans_report"
# # # link = r"C:\Users\jakna\Source\training_data\P785_no_meta\THICK"
# link = r'E:\scans\9YT4\THIN'
# folder_report(link, dir, reports_folder_name, plot_path = True,  image_tester = False, overwrite_blanks = True)

parser = argparse.ArgumentParser(description= 'Loop through all folders in a dir, find THICK and THIN folders and report on the scan contents')
parser.add_argument('-d', '--dir', type=str, metavar='', required=True, help= 'Link to main directory containing all scans')
parser.add_argument('-r', '--report', type=str, metavar='', required=True, help= 'Link to main directory to rebuild folder structure and write reports')
parser.add_argument('-p', '--plot', type=str2bool, nargs='?', metavar='', required=False, default=True, help= 'Plot paths of scans?')
parser.add_argument('-i', '--image_test', type=str2bool, nargs='?', metavar='', required=False, default=False, help= 'Test image success (slow)')
parser.add_argument('-o', '--overwrite_blanks', type=str2bool, nargs='?', metavar='', required=False, default=True, help= 'Overwrite empty metadata fields with estimated ones')


if __name__ == '__main__':
    args = parser.parse_args()
    report_all_folders(args.dir, args.report, args.plot, args.image_test, args.overwrite_blanks)