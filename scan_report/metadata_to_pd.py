import os
import json
import hashlib
import re
from numpy.lib.twodim_base import mask_indices
import yaml
import numpy as np
import piexif
import pandas as pd
from collections import Counter
from pathlib import Path
import datetime
from . import parameters

pd.options.mode.chained_assignment = None  # default='warn'

def will_it_float(value):
    try:
        float(value)
        return True
    except ValueError:
        return False

def pull_smear_type(im_link):
    if 'THIN' in Path(im_link).parts[-2:-1]:
        if 'THICK' in Path(im_link).parts[-2:-1]:
            print('Both styles?')
            return('BOTH?')
        else: return 'THIN'
    elif 'THICK' in Path(im_link).parts[-2:-1]: return 'THICK'
    else: return 'THICK' ### for now, we're told that if it's not specified, it's a thick scan

def pull_usercomment_dict(im_link, params):
    """
    Reads UserComment Exif data from a file, and returns the contained bytes as a pandas dataframe.
    If any keys in params aren't included, they are appended with value None
    Args:
        im_link: Path to the Exif-containing file
        params: A list of all the parameters we'd expect from a recent scan
    """
    exif_dict = piexif.load(im_link)
    if "Exif" in exif_dict and piexif.ExifIFD.UserComment in exif_dict["Exif"]:
        j = json.loads(exif_dict["Exif"][piexif.ExifIFD.UserComment].decode())
        my_dataframe = pd.json_normalize(j)
        
        my_dataframe.columns = my_dataframe.columns.str.replace(".", "_")

        for param in params:
            if param not in my_dataframe:
                my_dataframe[param] = None


        my_dataframe['image_acquisitionDate'][0] = datetime.datetime.fromisoformat(my_dataframe['image_acquisitionDate'][0]).strftime('%Y-%m-%d_%H-%M-%S')
        try:
            my_dataframe['dataset_acquisitionDate'][0] = datetime.datetime.fromisoformat(my_dataframe['dataset_acquisitionDate'][0]).strftime('%Y-%m-%d_%H-%M-%S')
        except:
            pass
        
        z_stack = hashlib.md5((str(my_dataframe['instrument_state_stage_position_x'][0]) + str(my_dataframe['instrument_state_stage_position_y'][0]) + str(my_dataframe['dataset_id'][0])).encode('utf-8')).hexdigest()
        my_dataframe['stack_id'] = z_stack
        
        my_dataframe['image_size'] = os.path.getsize(im_link)
        for k, v in my_dataframe.items():
            if v[0] is not None:
                if re.match(r'^[A-Z0-9]{4}$', str(v[0])) and k == 'image_annotations_{0}'.format(str(v[0])):
                    my_dataframe['dataset_patient_id'] = v[0]
                    break
        
        if my_dataframe['dataset_patient_id'][0] is None:
            if re.match(r'^[A-Z0-9]{4}$', Path(im_link).parts[-3]): my_dataframe['dataset_patient_id'] = Path(im_link).parts[-3]
            elif re.match(r'^[A-Z0-9]{4}$', my_dataframe['image_name'][0].split('_')[0]): my_dataframe['dataset_patient_id'] = my_dataframe['image_name'][0].split('_')[0]
            else: my_dataframe['dataset_patient_id'] = None
        my_dataframe['dataset_smear_type'] = pull_smear_type(im_link)
        return(my_dataframe)
    else:
        raise ValueError('JSON is empty')

def yaml_pull(im_link, params):
    """
    Reads UserComment Exif data from a file, and returns the contained bytes as a pandas dataframe.
    If any keys in params aren't included, they are appended with value None
    Args:
        im_link: Path to the Exif-containing file
        params: A list of all the parameters we'd expect from a recent scan
    """

    exif_dict = dict(yaml.safe_load(piexif.load(im_link)['Exif'][37510]))
    my_dataframe = pd.json_normalize(exif_dict)
    my_dataframe = my_dataframe.rename(columns = {
        "filename": "image_name",
        "format": "image_format",
        "id": "image_id",
        "tags": "image_tags",
        "time": "image_acquisitionDate", 
        "custom.Client": "image_annotations_Client",
        "custom.basename": "dataset_name",
        "custom.microscope_id": "instrument_settings_id",
        "custom.microscope_name": "instrument_settings_name",
        "custom.position.x": "instrument_state_stage_position_x",
        "custom.position.y": "instrument_state_stage_position_y",
        "custom.position.z": "instrument_state_stage_position_z",
        "custom.scan_id": "dataset_id",
        "custom.time": "dataset_acquisitionDate"
    })
    for param in params:
        if param not in my_dataframe:
            my_dataframe[param] = None
    my_dataframe['image_size'] = os.path.getsize(im_link)
    z_stack = hashlib.md5((str(my_dataframe['instrument_state_stage_position_x'][0]) + str(my_dataframe['instrument_state_stage_position_y'][0]) + str(my_dataframe['dataset_id'][0])).encode('utf-8')).hexdigest()
    my_dataframe['stack_id'] = z_stack
    my_dataframe['dataset_smear_type'] = pull_smear_type(im_link)
    
    for k, v in my_dataframe.items():
        if v[0] is not None:
            if re.match(r'^[A-Z0-9]{4}$', str(v[0])) and k == 'custom_{0}'.format(str(v[0])):
                my_dataframe['dataset_patient_id'] = v[0]
                break

    if my_dataframe['dataset_patient_id'][0] is None:
        if re.match(r'^[A-Z0-9]{4}$', Path(im_link).parts[-3]): my_dataframe['dataset_patient_id'] = Path(im_link).parts[-3]
        elif re.match(r'^[A-Z0-9]{4}$', my_dataframe['image_name'][0].split('_')[0]): my_dataframe['dataset_patient_id'] = my_dataframe['image_name'][0].split('_')[0]
        else: my_dataframe['dataset_patient_id'] = None
    return(my_dataframe)

def all_none(im_link, params):
    """
    If Exif data can't be read from a file, get basic info from the file name and location.
    If any keys in params aren't included, they are appended with value None
    Args:
        im_link: Path to the Exif-containing file
        params: A list of all the parameters we'd expect from a recent scan
    """
    file_name = os.path.basename(im_link)
    img_size = os.path.getsize(im_link)
    d = {'image_name':[file_name], 'image_size':[img_size]}
    my_dataframe = pd.DataFrame(data = d)
    if re.match(r'^[A-Z0-9]{4}$', Path(im_link).parts[-3]):
        my_dataframe['dataset_patient_id'] = Path(im_link).parts[-3]
    elif re.match(r'^[A-Z0-9]{4}$', my_dataframe['image_name'][0].split('_')[0]):
        my_dataframe['dataset_patient_id'] = my_dataframe['image_name'][0].split('_')[0]
    else: my_dataframe['dataset_patient_id'] = None
    my_dataframe['dataset_smear_type'] = pull_smear_type(im_link)
    for param in params:
        if param not in my_dataframe.keys():
            # print(param)
            my_dataframe[param] = None
    return(my_dataframe)

def pulldata(im_link, params = parameters.all_data):
    ''' Pull the metadata from the image linked into a pd.dataframe
        First try to import from a JSON format (the most recent and common),
        then YAML. If both fail, just generate the obvious metadata.
        Add the keys in params to the dataframe, with value None if they're not specified.
        Args:
            im_link: Path to the image to read
            params: a list of metadata keys to be appended to the dataframe
    '''

    try:
        data = pull_usercomment_dict(im_link, params)
        meta_style = 'JSON'
    except:
        try:
            data = yaml_pull(im_link, params)    
            meta_style = 'YAML'
        except:
            data = all_none(im_link, params)
            meta_style = 'empty'
    data.columns = data.columns.str.replace(".", "_")
    return(data, meta_style)

def estimate_image_metadata(im_link, metadata, replace_blanks):
    '''
    Estimates the metadata that could sensibly be extracted from a single image (so no movements or grid sizes), and appends it to the metadata with keys beginning with 'estimated_'
    These include patient ID from filename and directory, [x,y,z] position, file location, image format and dataset type.
    If replace_blanks is True, will set this estimated metadata in the accepted metadata fields, provided the pre-existing value was 0, None or []
    Args:
        im_link: Path to the image to read
        metadata: The metadata corresponding to the image in im_link
        replace_blanks: Boolean, whether or not blank metadata should be replaced by estimates
    '''
    changed = False

    if re.match(r'^[A-Z0-9]{4}$', metadata['image_name'][0][0:4]): metadata['estimated_patient_id_name'] = metadata['image_name'][0][0:4]

    name_list = metadata['image_name'][0][5:].split('_')
    if name_list[0] in ['THICK', 'THIN']: metadata['estimated_dataset_smear_type'] = name_list[0]
    if will_it_float(name_list[-3]): metadata['estimated_x'] = float(name_list[-3])
    if will_it_float(name_list[-2]): metadata['estimated_y'] = float(name_list[-2])
    if will_it_float(name_list[-1].split('.')[0]): metadata['estimated_z'] = float(name_list[-1].split('.')[0])

    if re.match(r'^[A-Z0-9]{4}$', Path(im_link).parts[-3]): metadata['estimated_patient_id_dir'] = Path(im_link).parts[-3]

    if name_list[-1].split('.')[1] in ['jpeg', 'jpg', 'png']: metadata['estimated_image_format'] = name_list[-1].split('.')[1]
    metadata['estimated_FileLocation'] = im_link
    metadata['estimated_dataset_type'] = 'xyzScan'
    metadata['estimated_image_annotations.{0}'.format(metadata['dataset_patient_id'][0])] = metadata['estimated_patient_id_name']

    metadata["estimated_image_acquisitionDate"] = datetime.datetime.utcfromtimestamp(os.path.getmtime(im_link)).strftime('%Y-%m-%d_%H-%M-%S')

    if replace_blanks is True:
        if metadata['dataset_smear_type'][0] in [None, 0, []]:
            metadata['dataset_smear_type'] = metadata['estimated_dataset_smear_type']
            changed = True
        if metadata['instrument_state_stage_position_x'][0] in [None, 0, []]:
            metadata['instrument_state_stage_position_x'] = metadata['estimated_x']
            changed = True
        if metadata['instrument_state_stage_position_y'][0] in [None, 0, []]:
            metadata['instrument_state_stage_position_y'] = metadata['estimated_y']
            changed = True
        if metadata['instrument_state_stage_position_z'][0] in [None, 0, []]:
            metadata['instrument_state_stage_position_z'] = metadata['estimated_z']
            changed = True

        if metadata['dataset_patient_id'][0] in [None, 0, []]:
            metadata['dataset_patient_id'] = metadata['estimated_patient_id_name']
            changed = True
        if metadata['FileLocation'][0] in [None, 0, []]:
            metadata['FileLocation'] = metadata['estimated_FileLocation']
            changed = True
        if metadata['dataset_type'][0] in [None, 0, []]:
            metadata['dataset_type'] =  metadata['estimated_dataset_type']
            changed = True
        
        if metadata['image_acquisitionDate'][0] in [None, 0, []]:
            metadata['image_acquisitionDate'] = metadata['estimated_image_acquisitionDate']
            changed = True

        if 'image_annotations.{0}'.format(metadata['dataset_patient_id'][0]) in metadata.keys():
            if metadata['image_annotations.{0}'.format(metadata['dataset_patient_id'][0])][0] in [None, 0, []]:
                metadata['image_annotations.{0}'.format(metadata['dataset_patient_id'][0])] =  metadata['dataset_patient_id']
                changed = True
        else:
            metadata['image_annotations.{0}'.format(metadata['dataset_patient_id'][0])] = metadata['dataset_patient_id']
            changed = True

    metadata['estimates_overwritten'] = changed

    return metadata